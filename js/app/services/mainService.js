angular.module('restaurantApp')

    .service('menuService', function () {
        var menu = [
            {
                id_: '0',
                name: 'Fire Roasted Vegetable',
                image: 'img/menu/Halian Roasted Vegetable.jpg',
                category: 'Food',
                label: 'Soups',
                price: '15.50',
                description: 'There’s nothing better than vegetable soup on a cold night, especially when it has the flavor of tons of fire roasted vegetables, couscous and a deep tomato based broth. Who knew all those vitamins could taste so good.',
                comment: ''
            },
            {
                id_: '1',
                name: 'Chassagne Montrachet',
                image: 'img/menu/Chassagne Montrachet.jpg',
                category: 'Food',
                label: 'Drinks',
                price: '6.17',
                description: 'This Chardonnay white wine is produced in the Chassagne-Montrachet. Aged 35 years on average, the vines benefit from clay-limestone soil. The wine is aged in French oak barrels (20% in new barrels) for 12 months, and is regularly stirred.',
                comment: ''
            },
            {
                id_: '2',
                name: 'Pizza Bake',
                image: 'img/menu/Pizza Bake.jpg',
                category: 'Food',
                label: 'Hot Dishes',
                price: '31.05',
                description: 'MMMM PIZZA!! Ok, so this healthy pizza bake isn’t really a substitute for Pizza but it is delicious. The base of this recipe is  spaghetti squash and it’s jam packed with lean ground turkey, Jennie-O hot Italian turkey sausage and veggies. It screams health!',
                comment: ''
            },
            {
                id_: '3',
                name: 'Low Carb Lasagna',
                image: 'img/menu/Low Carb Lasagna.jpg',
                category: 'Food',
                label: 'Hot Dishes',
                price: '21.34',
                description: 'This low carb lasagna recipe is packed with lean ground turkey, hot Italian turkey sausage and veggies! There are no noodles involved in this recipe so it is low carb and high protein! If you loved my meat sauce recipe (click here to get!) than you will love this low carb lasagna even more!',
            }

        ];
        this.getDishes = function () {
            return menu;
        }

        this.getDish = function (id) {
            return menu[id];
        }
    }

    );
