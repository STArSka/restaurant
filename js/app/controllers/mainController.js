angular.module('restaurantApp')

    .controller('menuController', ['$scope', 'menuService', function ($scope, menuService) {
        $scope.menu = menuService.getDishes();
        $scope.filterText = '';
        $scope.filter = function (text) {
            $scope.filterText = text;
        };

        $scope.isSelected = function (text) {
            return ($scope.filterText === text);
        }

        $scope.showComment = false;

        $scope.viewComment = function () {
            $scope.showComment = !$scope.showComment;
        }


    }])

    .controller('ContactController', ['$scope', function ($scope) {

        $scope.feedback = { mysubject: "", firstName: "", lastName: "", email: "", comments: "" };

        $scope.subjects = [{ value: "opinion", label: "Opinion" }, { value: "judgment", label: "Judgment" }, { value: "cooperation", label: "Сooperation" }, { value: "without", label: "Without subject" }];

        $scope.invalidSubjectSelection = false;

    }])

    .controller('FeedbackController', ['$scope', function ($scope) {

        $scope.sendFeedback = function () {

            console.log($scope.feedback);

            if ($scope.feedback.mysubject == "") {
                $scope.invalidSubjectSelection = true;
                console.log('Select a subject!');
            }
            else {

                $scope.invalidSubjectSelection = false;
                $scope.feedback = { mysubject: "", firstName: "", lastName: "", email: "", comments: "" };
                $scope.feedback.mysubject = "";
                $scope.feedbackForm.$setPristine();
            }
        };
    }])

    .controller('NewDishController', ['$scope', function ($scope) {

        $scope.newDish = {
            name: '', image: 'img/menu/Halian Roasted Vegetable.jpg',
            category: 'Food', label: '', price: '', description: ''
        };

        $scope.sendDish = function () {

            console.log($scope.newDish);

            $scope.menu.push($scope.newDish);
            $scope.dishForm.$setPristine();
            $scope.newDish = {
                name: '', image: 'img/menu/Halian Roasted Vegetable.jpg',
                category: 'Food', label: '', price: '', description: ''
            };
        }
    }])

    .controller('dishInfoController', ['$scope', '$stateParams', 'menuService',
        function ($scope, $stateParams, menuService) {

            $scope.dish = menuService.getDish(parseInt($stateParams.id, 10));

        }])