angular.module('restaurantApp', ['ngAnimate', 'ui.bootstrap', 'ui.router'])
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider

			.state('app', {
				url: '/',
				views: {
					'header': {
						templateUrl: 'views/header.html',
					},
					'content': {
						templateUrl: 'views/about.html',
					},
					'footer': {
						templateUrl: 'views/footer.html',
					}
				}

			})

			.state('app.about', {
				url: 'about',
				views: {
					'content@': {
						templateUrl: 'views/about.html',
					}
				}
			})

			.state('app.callus', {
				url: 'callus',
				views: {
					'content@': {
						templateUrl: 'views/callus.html',
						controller: 'ContactController'
					}
				}
			})

			.state('app.menu', {
				url: 'menu',
				views: {
					'content@': {
						templateUrl: 'views/menu.html',
						controller: 'menuController'
					}
				}
			})

			.state('app.dishinfo', {
				url: 'books/:id',
				views: {
					'content@': {
						templateUrl: 'views/dishInfo.html',
						controller: 'dishInfoController'
					}
				}
			});

		$urlRouterProvider.otherwise('/');
	})
